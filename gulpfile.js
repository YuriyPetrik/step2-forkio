const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cleancss = require('gulp-clean-css')
const clean = require('gulp-clean');
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");
const minify = require("gulp-minify");
const deleteFiles = require("delete");
const sass = require("gulp-sass")(require("sass"));

gulp.task("html-minify", function () {
    return gulp
        .src("src/html/*.html")
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest("dist"));
});

gulp.task("distImg", function () {
    return gulp.src("src/img/**/*").pipe(gulp.dest("dist/img"));
});

gulp.task("distJs", function () {
    return gulp.src("src/js/*.js")
        .pipe(minify()).pipe(gulp.dest("dist"))

});

gulp.task("deleteFiles", function () {
    return deleteFiles("dist/");
});

gulp.task("dist-css", function () {
    return (
        gulp
            .src("src/scss/main.scss")
            .pipe(
                autoprefixer({
                    cascade: false,
                })
            )
            .pipe(sass().on("error", sass.logError))
            // .pipe(concat("styles.css"))
            .pipe(cssmin())
            .pipe(rename({ suffix: ".min" }))
            .pipe(cleancss())
            .pipe(gulp.dest("dist"))
    );
});
const cleanBuild = () => (
    gulp.src('build/', {allowEmpty: true})
        .pipe(clean())
);

gulp.task('build', gulp.series(
    cleanBuild,
    gulp.parallel("html-minify", "distJs", "dist-css", "distImg")
));

gulp.task(
    "dist",
    gulp.series(
        "deleteFiles",
        gulp.parallel("html-minify", "distJs", "dist-css", "distImg")
    )
);
gulp.watch(
    ["src/scss/**/**/*.scss", "src/html/*.html", "src/js/*.js"],
    gulp.series("dist")
);
